﻿namespace ForumScraperApi.Models;

public record struct MovieInfo(string Title, int Year);
