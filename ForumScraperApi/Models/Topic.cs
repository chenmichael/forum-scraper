﻿using System.Text.Json.Serialization;

namespace ForumScraperApi.Models;

public class Topic {
    [JsonPropertyName("topicId")] public required int Id { get; set; }
    [JsonPropertyName("title")] public required string Title { get; set; }
    [JsonPropertyName("forumId")] public required int ForumId { get; set; }
    [JsonPropertyName("postCount")] public required int PostCount { get; set; }
    [JsonPropertyName("viewCount")] public required int ViewCount { get; set; }
    [JsonPropertyName("timestamp")] public required DateTimeOffset Timestamp { get; set; }

    [JsonIgnore] public required int AuthorId { get; set; }
    [JsonPropertyName("author")] public Author Author { get; set; } = null!;

    [JsonIgnore] public int? MovieId { get; set; } = null;
    [JsonPropertyName("movie")] public Movie Movie { get; set; } = null!;

    public TopicDto ToDto()
        => new(Title, Id, ForumId, PostCount, ViewCount, Author.ToDto(), Timestamp);
}
