﻿using System.Text.Json.Serialization;

namespace ForumScraperApi.Models;

public class Author {
    [JsonPropertyName("id")] public required int Id { get; set; }
    [JsonPropertyName("name")] public required string Name { get; set; }

    [JsonIgnore] public ICollection<Topic> Topics { get; set; } = null!;

    public AuthorDto ToDto() => new(Id, Name);
}
