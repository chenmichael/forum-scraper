﻿using System.Text.Json.Serialization;

namespace ForumScraperApi.Models;

public record AuthorDto(
    [property: JsonPropertyName("id")] int Id,
    [property: JsonPropertyName("name")] string Name) {
    public Author ToAuthor() => new() {
        Id = Id,
        Name = Name,
    };
}
