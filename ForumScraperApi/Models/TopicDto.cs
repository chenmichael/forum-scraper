﻿using System.Text.Json.Serialization;

namespace ForumScraperApi.Models;

public record TopicDto(
    [property: JsonPropertyName("title")] string Title,
    [property: JsonPropertyName("forumId")] int ForumId,
    [property: JsonPropertyName("topicId")] int TopicId,
    [property: JsonPropertyName("postCount")] int PostCount,
    [property: JsonPropertyName("viewCount")] int ViewCount,
    [property: JsonPropertyName("author")] AuthorDto Author,
    [property: JsonPropertyName("timestamp")] DateTimeOffset Timestamp) {
    public Topic ToTopic(Movie? movie) => new() {
        Id = TopicId,
        Title = Title,
        AuthorId = Author.Id,
        ForumId = ForumId,
        PostCount = PostCount,
        Timestamp = Timestamp,
        ViewCount = ViewCount,
        MovieId = movie?.Id,
    };
}
