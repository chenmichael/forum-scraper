﻿using System.Text.Json.Serialization;

namespace ForumScraperApi.Models;

public class Movie {
    [JsonPropertyName("id")] public required int Id { get; set; }
    [JsonPropertyName("title")] public required string Title { get; set; }
    [JsonPropertyName("year")] public required int Year { get; set; }

    [JsonIgnore] public ICollection<Topic> Topics { get; set; } = null!;
}
