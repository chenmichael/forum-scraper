﻿using System.ComponentModel.DataAnnotations;
using ForumScraperApi.Interfaces;
using ForumScraperApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using TMDbLib.Client;

namespace ForumScraperApi.Scraper;

public class TMDbScraper(
    ILogger<TMDbScraper> logger,
    IOptions<TMDbScraper.Options> options,
    ITitleParser titleParser,
    ScraperContext dbContext) : IScraper {
    public TMDbClient Client => new(options.Value.ApiKey);

    private static readonly Dictionary<string, Movie?> _cache = [];

    public async Task<Movie?> IdentifyMovieAsync(string title, CancellationToken cancellationToken) {
        if (_cache.TryGetValue(title, out var known))
            return known;
        if (titleParser.ParseTitle(title) is not { } movie) {
            logger.LogWarning("Could not determine title for topic '{topic}'.", title);
            return null;
        }
        var result = await Client.SearchMovieAsync(movie.Title, primaryReleaseYear: movie.Year, cancellationToken: cancellationToken);
        if (result.TotalResults is 0)
            logger.LogWarning("No matches for {movie}.", movie);
        else
            logger.LogInformation("Found {matches} matches for {movie}.", result.TotalResults, movie);
        var selectedMovie = result.Results.FirstOrDefault();
        var info = selectedMovie is null ? null : new Movie {
            Id = selectedMovie.Id,
            Title = selectedMovie.Title,
            Year = movie.Year,
        };
        _cache[title] = info;
        if (info is not null) {
            if (await dbContext.Movies.FirstOrDefaultAsync(i => i.Id == info.Id, cancellationToken) is { } existing)
                dbContext.Movies.Entry(existing).CurrentValues.SetValues(info);
            else
                await dbContext.Movies.AddAsync(info, cancellationToken);
            await dbContext.SaveChangesAsync(cancellationToken);
        }
        return info;
    }
    public class Options {
        public const string Key = "TMDb";

        [Required, MinLength(10)]
        public required string ApiKey { get; set; }
    }
}
