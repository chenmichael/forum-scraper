using ForumScraperApi;
using ForumScraperApi.Interfaces;
using ForumScraperApi.Scraper;
using ForumScraperApi.TitleParser;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IScraper, TMDbScraper>();
builder.Services.AddSingleton<ITitleParser, SnahpTitleParser>();

builder.Services.AddOptions<TMDbScraper.Options>()
    .BindConfiguration(TMDbScraper.Options.Key)
    .ValidateDataAnnotations()
    .ValidateOnStart();

builder.Services.AddControllers();
builder.Services.Configure<RouteOptions>(options => {
    options.LowercaseUrls = true;
    options.LowercaseQueryStrings = true;
});

Directory.CreateDirectory("database");
builder.Services.AddSqlite<ScraperContext>(new SqliteConnectionStringBuilder {
    DataSource = "database/scraper.db",
}.ConnectionString);

var app = builder.Build();

using (var scope = app.Services.CreateScope()) {
    var dbContext = scope.ServiceProvider.GetRequiredService<ScraperContext>();
    await dbContext.Database.MigrateAsync();
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapControllers();

app.Run();
