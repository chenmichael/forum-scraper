﻿using ForumScraperApi.Models;

namespace ForumScraperApi.Interfaces;

public interface ITitleParser {
    MovieInfo? ParseTitle(string title);
}
