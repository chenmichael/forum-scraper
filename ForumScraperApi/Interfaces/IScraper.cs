﻿using ForumScraperApi.Models;

namespace ForumScraperApi.Interfaces;

public interface IScraper {
    Task<Movie?> IdentifyMovieAsync(string title, CancellationToken cancellationToken);
}
