﻿using System.Text.Json;
using ForumScraperApi.Interfaces;
using ForumScraperApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ForumScraperApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class TopicsController(
    ILogger<TopicsController> logger,
    ScraperContext dbContext,
    IScraper scraper) : ControllerBase {

    [HttpPost("scrape")]
    public async Task<IActionResult> ScrapeAsync(CancellationToken cancellationToken) {
        var topics = await JsonSerializer.DeserializeAsync<IList<TopicDto>>(Request.Body, cancellationToken: cancellationToken);
        if (topics is null)
            return BadRequest("No topics were provided!");
        logger.LogInformation("Submitted {amount} topics!", topics.Count);

        foreach (var author in topics
            .Select(i => i.Author)
            .GroupBy(i => i.Id)
            .Select(i => i.First())
            .Select(i => i.ToAuthor()))
            if (await dbContext.Authors.FirstOrDefaultAsync(i => i.Id == author.Id, cancellationToken) is { } existing)
                dbContext.Authors.Entry(existing).CurrentValues.SetValues(author);
            else
                await dbContext.Authors.AddAsync(author, cancellationToken);

        foreach (var dtoTopic in topics) {
            var movie = await scraper.IdentifyMovieAsync(dtoTopic.Title, cancellationToken);
            var topic = dtoTopic.ToTopic(movie);

            if (await dbContext.Topics.FirstOrDefaultAsync(i => i.Id == topic.Id, cancellationToken) is { } existing)
                dbContext.Topics.Entry(existing).CurrentValues.SetValues(topic);
            else
                await dbContext.Topics.AddAsync(topic, cancellationToken);
        }

        var changes = await dbContext.SaveChangesAsync(cancellationToken);
        logger.LogInformation("{changes} changes written!", changes);
        return Ok($"{topics.Count} topics read, {changes} changes written!");
    }
}
