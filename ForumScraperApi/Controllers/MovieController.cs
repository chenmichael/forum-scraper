﻿using ForumScraperApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ForumScraperApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class MovieController(
    ScraperContext dbContext) : ControllerBase {
    [HttpGet("{movieId}/topics")]
    public async Task<IEnumerable<TopicDto>> GetTopicsAsync(int movieId, CancellationToken cancellationToken) {
        var topics = await dbContext.Topics
            .Include(i => i.Author)
            .Where(i => i.MovieId == movieId)
            .OrderByDescending(i => i.ViewCount)
            .ToListAsync(cancellationToken);
        return topics.Select(i => i.ToDto());
    }
}
