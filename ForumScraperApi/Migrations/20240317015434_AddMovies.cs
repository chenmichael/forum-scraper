﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ForumScraperApi.Migrations;

/// <inheritdoc />
public partial class AddMovies : Migration {
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder) {
        migrationBuilder.AddColumn<int>(
            name: "MovieId",
            table: "Topics",
            type: "INTEGER",
            nullable: true);

        migrationBuilder.CreateTable(
            name: "Movies",
            columns: table => new {
                Id = table.Column<int>(type: "INTEGER", nullable: false)
                    .Annotation("Sqlite:Autoincrement", true),
                Title = table.Column<string>(type: "TEXT", nullable: false),
                Year = table.Column<int>(type: "INTEGER", nullable: false)
            },
            constraints: table => {
                table.PrimaryKey("PK_Movies", x => x.Id);
            });

        migrationBuilder.CreateIndex(
            name: "IX_Topics_MovieId",
            table: "Topics",
            column: "MovieId");

        migrationBuilder.AddForeignKey(
            name: "FK_Topics_Movies_MovieId",
            table: "Topics",
            column: "MovieId",
            principalTable: "Movies",
            principalColumn: "Id");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder) {
        migrationBuilder.DropForeignKey(
            name: "FK_Topics_Movies_MovieId",
            table: "Topics");

        migrationBuilder.DropTable(
            name: "Movies");

        migrationBuilder.DropIndex(
            name: "IX_Topics_MovieId",
            table: "Topics");

        migrationBuilder.DropColumn(
            name: "MovieId",
            table: "Topics");
    }
}
