﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ForumScraperApi.Migrations;
/// <inheritdoc />
public partial class InitialCreate : Migration {
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder) {
        migrationBuilder.CreateTable(
            name: "Authors",
            columns: table => new {
                Id = table.Column<int>(type: "INTEGER", nullable: false)
                    .Annotation("Sqlite:Autoincrement", true),
                Name = table.Column<string>(type: "TEXT", nullable: false)
            },
            constraints: table => {
                table.PrimaryKey("PK_Authors", x => x.Id);
            });

        migrationBuilder.CreateTable(
            name: "Topics",
            columns: table => new {
                Id = table.Column<int>(type: "INTEGER", nullable: false)
                    .Annotation("Sqlite:Autoincrement", true),
                Title = table.Column<string>(type: "TEXT", nullable: false),
                ForumId = table.Column<int>(type: "INTEGER", nullable: false),
                PostCount = table.Column<int>(type: "INTEGER", nullable: false),
                ViewCount = table.Column<int>(type: "INTEGER", nullable: false),
                Timestamp = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                AuthorId = table.Column<int>(type: "INTEGER", nullable: false)
            },
            constraints: table => {
                table.PrimaryKey("PK_Topics", x => x.Id);
                table.ForeignKey(
                    name: "FK_Topics_Authors_AuthorId",
                    column: x => x.AuthorId,
                    principalTable: "Authors",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
            });

        migrationBuilder.CreateIndex(
            name: "IX_Topics_AuthorId",
            table: "Topics",
            column: "AuthorId");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder) {
        migrationBuilder.DropTable(
            name: "Topics");

        migrationBuilder.DropTable(
            name: "Authors");
    }
}
