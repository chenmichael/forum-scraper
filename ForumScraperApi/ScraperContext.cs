﻿using ForumScraperApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ForumScraperApi;

public class ScraperContext(DbContextOptions options) : DbContext(options) {
    public DbSet<Topic> Topics => Set<Topic>();
    public DbSet<Author> Authors => Set<Author>();
    public DbSet<Movie> Movies => Set<Movie>();
}
