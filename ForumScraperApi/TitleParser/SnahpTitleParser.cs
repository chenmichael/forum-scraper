﻿using System.Text.RegularExpressions;
using ForumScraperApi.Interfaces;
using ForumScraperApi.Models;

namespace ForumScraperApi.TitleParser;

public partial class SnahpTitleParser(ILogger<SnahpTitleParser> logger) : ITitleParser {
    public MovieInfo? ParseTitle(string title) {
        if (YearRegex().Match(title) is not { Success: true } match)
            return null;
        var year = int.Parse(match.ValueSpan);
        var end = match.Index;

        var titleSpan = title.AsSpan()[..end];
        Span<char> nameSpan = stackalloc char[titleSpan.Length];
        var nameLength = 0;
        var lastSpace = true;
        for (var i = 0; i < titleSpan.Length; i++) {
            var titleChar = titleSpan[i];
            if (char.IsAsciiLetterOrDigit(titleChar)) {
                nameSpan[nameLength++] = titleChar;
                lastSpace = false;
            } else if (!lastSpace) {
                nameSpan[nameLength++] = ' ';
                lastSpace = true;
            }
        }
        var name = nameSpan[..nameLength].ToString();

        var info = new MovieInfo(name, year);
        logger.LogDebug("Determined {info} from '{title}'.", info, title);
        return info;
    }

    [GeneratedRegex(@"(?:19|20)\d{2}")]
    private static partial Regex YearRegex();
}