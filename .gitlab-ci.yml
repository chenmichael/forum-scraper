image: mcr.microsoft.com/dotnet/sdk:8.0

default:
  tags:
    - docker

# Prevent duplicate pipelines (see https://gitlab.com/gitlab-org/gitlab/-/issues/300146)
workflow:
  rules:
    # forbid merge request pipelines
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - when: always

variables:
  PROJECT_NAME:
    description: The name of the main .NET project.
    value: ForumScraperApi
  BUILD_CONFIGURATION:
    description: The build configuration to test and deploy.
    value: Release
  DOCKERFILE_PATH:
    description: Specify the path of the Dockerfile.
    value: ${PROJECT_NAME}/Dockerfile
    expand: true
  DOCKER_CONTEXT:
    description: Specify the path of the docker build context.
    value: "."
    expand: true
  DOCKER_TLS_CERTDIR:
    description: Specify to Docker where to create the certificates. (Requires volume mount in runner config)
    value: /certs
  TARGET_PLATFORMS:
    description: Specify the Docker image target platforms.
    value: linux/amd64,linux/arm64/v7,linux/arm64/v8
  BUILDER_NAME:
    description: Specify the name of the docker builder instance.
    value: dotnetbuilder
  CONTEXT_NAME:
    description: Specify the name of the docker build context.
    value: dotnetcontext

# Run for all tags
.is-tag:
  rules:
    - &is-tag
      if: $CI_COMMIT_TAG

# Prevent running if tag is not set
.only-tags:
  rules:
    - &only-tags
      if: $CI_COMMIT_TAG == null
      when: never

# Run for all branches
.is-branch:
  rules:
    - &is-branch
      if: $CI_COMMIT_BRANCH

# Prevent running if branch is not set
.only-branches:
  rules:
    - &only-branches
      if: $CI_COMMIT_BRANCH == null
      when: never

# Run only for protected tags
.prevent-unprotected:
  rules:
    - &prevent-unprotected
      if: $CI_COMMIT_REF_PROTECTED != "true"
      when: never

# Run on protected default branch but prevent duplicate release
.default-branch:
  rules:
    - &default-branch
      if: $CI_COMMIT_REF_PROTECTED && ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH) && ($CI_COMMIT_TITLE !~ /\[release\]/i)

# Run only if a test project exists
.has-testproject:
  rules:
    - &has-test-project
      exists:
        - "*.Tests/*.csproj"

.has-dockerfile:
  rules:
    - &has-dockerfile
      when: on_success
      # exists:
      #   - ${DOCKERFILE_PATH}

build-job:
  stage: build
  rules:
    - when: always
  script:
    - dotnet build --configuration "${BUILD_CONFIGURATION}"
  artifacts:
    untracked: true
    expire_in: 1 hour

test-job:
  stage: test
  variables:
    COVERAGE_COLLECTOR: XPlat Code Coverage
    LOG_WRITER: junit;LogFilePath=TestResults/testresult.xml;MethodFormat=Class;FailureBodyFormat=Verbose
    COVERLET_SETTINGS: coverlet.runsettings
  rules:
    - *has-test-project
  needs:
    - job: build-job
  script:
    - dotnet test
      --configuration "${BUILD_CONFIGURATION}"
      --results-directory "cobertura"
      --collect "${COVERAGE_COLLECTOR}"
      --logger "${LOG_WRITER}"
      --settings "${COVERLET_SETTINGS}"
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: cobertura/*/coverage.cobertura.xml
      junit:
        - ${PROJECT_NAME}.Tests/TestResults/testresult.xml
    paths:
      - cobertura/
    expire_in: 1 hour

coveragereport-job:
  stage: test
  rules:
    - *has-test-project
  needs:
    - job: test-job
  allow_failure: true
  image: registry.gitlab.com/chenmichael/reportgenerator:5
  script:
    - reportgenerator "-reports:cobertura/*/coverage.cobertura.xml" "-targetdir:coveragereport" "-reporttypes:Html_Dark;TextSummary"
    # write report text summary to stdout for coverage extraction
    - sed -n '/Summary/,/^$/p' coveragereport/Summary.txt
  coverage: '/Line coverage: \d+(?:\.\d+)?%?/'
  environment:
    name: coverage/$CI_COMMIT_REF_SLUG
    deployment_tier: other
    url: https://${CI_PROJECT_NAMESPACE}.${CI_PAGES_DOMAIN}/-/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/coveragereport/index.html
    auto_stop_in: 1 month
  artifacts:
    paths:
      - coveragereport/
    expire_in: 1 hour

releaser:
  stage: deploy
  rules:
    # Select release branch
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run only on protected branches (required for protected CI variables)
    - if: $CI_COMMIT_REF_PROTECTED != "true"
      when: never
  image:
    name: registry.gitlab.com/chenmichael/releaser:dev
  script: echo "Releasing new version"
  variables:
    # Specify releaser variables
    RELEASER_PACKAGEFILE: NorthScore/NorthScore.csproj
    GIT_STRATEGY: none

tag-analysis:
  image: registry.gitlab.com/chenmichael/analyzesemver:1
  stage: build
  rules:
    - *is-tag
  script:
    - analyze --no-prerelease "${CI_COMMIT_TAG}" | tee tag.env
  artifacts:
    reports:
      dotenv:
        - tag.env
    expire_in: never

.docker-job:
  stage: build
  # Use the official docker image.
  image: docker:24-git
  tags:
    - docker
    - privileged
  rules:
    - *prevent-unprotected
    - *has-dockerfile
  services:
    - docker:24-dind
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    - docker context inspect "${CONTEXT_NAME}" || docker context create "${CONTEXT_NAME}"
    - docker buildx inspect "${BUILDER_NAME}" || docker buildx create --name "${BUILDER_NAME}" --platform "${TARGET_PLATFORMS}" --use "${CONTEXT_NAME}"

docker-tag-build:
  extends:
    - .docker-job
  rules:
    # Run on protected branches if a dockerfile exists
    - *prevent-unprotected
    - *only-tags
    - *has-dockerfile
  needs:
    - job: tag-analysis
  script:
    - docker buildx build
      --pull
      --platform "${TARGET_PLATFORMS}"
      --tag "${CI_REGISTRY_IMAGE}"
      --tag "${CI_REGISTRY_IMAGE}:${RELEASE_MAJOR}"
      --tag "${CI_REGISTRY_IMAGE}:${RELEASE_MAJOR_MINOR}"
      --tag "${CI_REGISTRY_IMAGE}:${RELEASE_MAJOR_MINOR_PATCH}"
      --tag "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
      --file "${DOCKERFILE_PATH}"
      --provenance false
      --push "${DOCKER_CONTEXT}"

docker-branch-build:
  extends:
    - .docker-job
  rules:
    # Run on protected branches if a dockerfile exists
    - *prevent-unprotected
    - *only-branches
    - *has-dockerfile
  script:
    - docker buildx build
      --pull
      --platform "${TARGET_PLATFORMS}"
      --tag "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}"
      --tag "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
      --file "${DOCKERFILE_PATH}"
      --provenance false
      --push "${DOCKER_CONTEXT}"
