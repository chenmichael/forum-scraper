// ==UserScript==
// @name         Forum Scraper
// @namespace    http://tampermonkey.net/
// @version      2024-03-16
// @description  Scrapes the forum for links
// @author       Michael Chen
// @match        https://fora.snahp.eu/*
// @grant        none
// ==/UserScript==

(async function () {
  "use strict";

  /**
   * @param {URL} url
   * @param {string} name
   */
  function intParam(url, name) {
    const param = url.searchParams.get(name);
    if (param === null)
      throw new Error(`Required topic param ${JSON.stringify(name)} missing!`);
    return Number.parseInt(param);
  }

  /**
   * @param {Element} element
   * @param {string} name
   */
  function extractInteger(element, name) {
    const descriptor = element.querySelector(`.${name}`);
    if (descriptor === null)
      throw new Error(`Required topic descriptor with class ${name} missing!`);
    const text = descriptor.textContent;
    if (text === null)
      throw new Error(`No text in topic descriptor with class ${name}!`);
    return Number.parseInt(text);
  }

  /**
   * @param {Element} element
   * @returns {Author}
   */
  function parseAuthorElement(element) {
    const hrefUrl = element.getAttribute("href");
    if (hrefUrl === null) throw new Error("No author URL!");
    const url = new URL(hrefUrl, window.location.href);

    if (url.searchParams.get("mode") !== "viewprofile")
      throw new Error("Author URL is invalid!");

    const id = intParam(url, "u");
    const name = element.textContent;
    if (name === null) throw new Error("No author name!");
    return { id, name };
  }

  /**
   * @param {NodeListOf<ChildNode>} children
   */
  function findDateNode(children) {
    for (const child of children) {
      if (child.nodeType !== Node.TEXT_NODE) continue;
      let text = child.textContent;
      if (text === null) continue;
      const regex =
        /(?<day>\d+)\s+(?<month>jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)\s+(?<year>\d{4})\s*,\s*(?<hour>\d{1,2}):(?<minute>\d{1,2})/i;
      const m = regex.exec(text);
      if (m === null) continue;
      const parsed = Date.parse(m[0]);
      if (isNaN(parsed)) continue;
      return new Date(parsed);
    }
    console.log(children);
    throw new Error("No date containing child nodes found!");
  }

  /**
   * @param {Element} element
   */
  function parseDate(element) {
    const children = element.childNodes;
    return findDateNode(children);
  }

  /**
   * @param {Element} element
   * @returns {Post}
   */
  function parseTopicElement(element) {
    const titleElement = element.querySelector(".topictitle");
    if (titleElement === null) throw new Error("No title element!");

    const title = titleElement.textContent;
    if (title === null) throw new Error("Title is null!");

    const hrefUrl = titleElement.getAttribute("href");
    if (hrefUrl === null) throw new Error("No post URL!");
    const url = new URL(hrefUrl, window.location.href);
    const forumId = intParam(url, "f");
    const topicId = intParam(url, "t");
    const postCount = extractInteger(element, "posts");
    const viewCount = extractInteger(element, "views");

    const posterElement = element.querySelector(".topic-poster");
    if (posterElement === null) throw new Error("Topic has no poster element!");

    const timestamp = parseDate(posterElement);

    const authorElement = posterElement.querySelector(".username-coloured");
    if (authorElement === null)
      throw new Error("Poster has no author element!");
    const author = parseAuthorElement(authorElement);

    return { title, forumId, topicId, postCount, viewCount, author, timestamp };
  }

  const topicElements = document.querySelectorAll(".topics > li > dl");
  /** @type {Post[]} */
  const topics = [];
  for (const topicElement of topicElements)
    topics.push(parseTopicElement(topicElement));

  if (topics.length === 0) {
    console.log("No topics found!");
    return;
  }

  await fetch("http://localhost:5183/api/topics/scrape", {
    method: "POST",
    mode: "no-cors",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(topics),
  });
})();
