interface Post {
  title: string;
  forumId: int;
  topicId: int;
  postCount: int;
  viewCount: int;
  author: Author;
  timestamp: Date;
}

interface Author {
  id: int;
  name: string;
}
